# GridWorld Problem 

In this repository is the proof of the problem GridWorld from "Reinforcement Learning: An Introduction" by Richard S. Sutton and Andrew G. Barton.
In order to find the value-state function of the grid we will follow the Bellman equation which states:

```math
v_\pi(s) = \sum_a\pi(a|s)\sum_{r, s'}p(s',r|a, s)[r + \gamma v_\pi(s')]
```
Where $`\gamma`$ represents the discount factor, $`\pi(a|s)`$ is the policy by which the agents obeys and p(s',r|a, s) is transition probability in state s' with the reward r from state s making an action a.

The action space represents all 4 possible moves that can be done by the agent, i.e("up,down, left, right").The policy for every action will be always 0.25(as said in the problem)

The state of the agent will simply be the (x,y) coordinates on the grid.
I've defined a function called step that returns the next state and the reward, given the current state and action

```python
def step(action, state): # -> reward, next_state 
    if state == A: 
        return +10, A_PRIME
    if state == B:
        return +5, B_PRIME

    next_state = np.array(state) + action
    x,y = next_state

    if x < 0 or y < 0 or x > 4 or y > 4:
        next_state = state               
        reward = -1                     #out of grid state
    else:
        reward = 0

    return reward, next_state 
```

In our case the probability transition will always be 100%, i.e there is an 100% probability that if the agents takes the action of moving up he will end up in the block above the current state.

Knowing that, we can iterate over all possible actions acording to Bellman's equation and fing the value-state function of each block of the grid.
```python
for action in ACTION_SPACE:
    reward, (new_x, new_y)  = step(action,[i, j])
    #bellman equation
    current_grid[i,j] += POLICY*(reward + DISCOUNT*grid[new_x][new_y])
```
Resulting in a tabel like the one in the book:
```bash
[[ 3.3  8.8  4.4  5.3  1.5]
 [ 1.5  3.   2.3  1.9  0.6]
 [ 0.1  0.7  0.7  0.4 -0.4]
 [-1.  -0.4 -0.4 -0.6 -1.2]
 [-1.9 -1.3 -1.2 -1.4 -2. ]]
```

TODO:
- [x] find the value-state function of the grid
- [ ] Find the optimal policy 
