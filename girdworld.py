import numpy as np 

DISCOUNT = 0.9

A = [0,1]
A_PRIME = [4,1]
B = [0,3]
B_PRIME = [2,3]

ACTION_SPACE = [np.array([+1,0]), np.array([-1,0]), np.array([0,+1]), np.array([0,-1])]
POLICY = 0.25


WORLD_DIM = 5

def step(action, state): # -> reward, next_state 
    if state == A:
        return +10, A_PRIME

    if state == B:
        return +5, B_PRIME

    next_state = np.array(state) + action
    x,y = next_state

    if x < 0 or y < 0 or x > 4 or y > 4:
        next_state = state
        reward = -1
    else:
        reward = 0

    return reward, next_state 
    
def find_state_value_function():
    grid = np.zeros((WORLD_DIM, WORLD_DIM))
    converged = False

    while not converged:
        current_grid = np.zeros_like(grid)
        
        for i in range(WORLD_DIM):
            for j in range(WORLD_DIM):
                for action in ACTION_SPACE:
                    reward, (new_x, new_y)  = step(action,[i, j])
                    #bellman equation
                    current_grid[i,j] += POLICY*(reward + DISCOUNT*grid[new_x][new_y])

        if np.sum(np.abs(grid - current_grid)) <= 1e-2:
            converged = True
        grid = current_grid

    print(np.round(grid, 1))

if __name__ == '__main__':
    find_state_value_function()